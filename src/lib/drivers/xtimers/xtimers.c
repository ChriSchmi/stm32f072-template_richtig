/**
 *****************************************************************************
 * @title   timers.c
 * @author  Daniel Schnell <dschnell@posteo.de>
 * @brief
 *****************************************************************************
 */

/* Includes ------------------------------------------------------------------*/

#include <stdbool.h>

#include "stm32f0xx.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_tim.h"
#include "stm32f0xx_dma.h"
#include "stm32f0xx_misc.h"

#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"

#include "xtimers.h"
#include "logger.h"
#include "string.h"

/* definitions */

#undef XTIMERS_DEBUG
#undef XTIMERS_EXTREME_DEBUG
//#define XTIMERS_DEBUG   1
//#define XTIMERS_EXTREME_DEBUG   1
#ifdef XTIMERS_DEBUG
#include "dbg_pin.h"
#endif

/* typedefs */

/* rgb buffer structure */
typedef struct
{
    const COLOR_RGB_TYPE_T*  rgb_buf;        //< RGB value buffer
    size_t                   rgb_buf_size;   //< size of RGB value buffer in elements
    size_t                   rgb_buf_pos;    //< act. position in RGB value value
} rgb_buf_t;


typedef struct xtimer_dma_s
{
    TIM_TypeDef*            timer;          //< Timer to be used, e.g. TIM1, TIM2, ...
    DMA_Channel_TypeDef*    chnl;           //< DMA channel
    enum IRQn               irq_chnl;       //< IRQ Channel
    uint16_t*               buf;            //< buffer to be used for DMA transfer,
                                            //< if NULL it will be allocated
    size_t                  n_leds;         //< number of leds currently used, set by xtimers_start()
    size_t                  n_leds_max;     //< max number of led's used for allocation,
                                            //< must not be 0 and be divisible by 2
    uint32_t                dest;           //< destination of DMA transfer (16 bit CCRx register)
    uint32_t                prio;           //< DMA priority
    rgb_buf_t*              src;            //< source buffer
} xtimer_dma_t;



/* global variables */
static xSemaphoreHandle        xtimer_sem;     //< synchronization between DMA IRQ & user space call
static xSemaphoreHandle        xtimer_mutex;   //< guarantee singular access

static rgb_buf_t rgb_buf_desc =
{
    .rgb_buf = NULL,                //< RGB value buffer
    .rgb_buf_size = 0,              //< size of RGB value buffer
    .rgb_buf_pos = 0                //< actual position in RGB value buffer
};


static xtimer_dma_t xdma1 =
{
        .timer          = TIM17,
        .chnl           = DMA1_Channel1,
        .irq_chnl       = DMA1_Channel1_IRQn,
        /** We need to take number of LED's in consideration for choosing correct
         * value of buffer size here. For 60 LED's we'd use 528 bytes according to
         * size of buffer for 22 LED's. Therefore we need 3 rounds of circular DMA
         * with time for theoretically 66 LED's. That is 4 LED times more (== 120us)
         * than necessary dead time for reaching our 2 LED settle time for ws2811.
         * It limits max. frequency we can alter LED settings: 66 LED times: ~2ms ~505 Hz
         * in comparison to 62 LED times: ~1,8ms ~537 Hz for optimal settings
         */
        .n_leds         = 8,      //< number of leds
        .n_leds_max     = 8,      //< max number of led's used for allocation
        .buf            = NULL,
        .dest           = (uint32_t) &TIM17->CCR1,
        .prio           = DMA_Priority_Medium,
        .src            = &rgb_buf_desc
};

/* Forward function declarations */

static void xtimers_dma_irq_init(xtimer_dma_t* xdma);

/* Function definitions */


/**
 * Update DMA values. Afterwards DMA can be started
 * with  DMA_Cmd(xdma->chnl, ENABLE)
 *
 * @param xdma
 * @param circular
 *
 */
static void xtimers_update_dma(xtimer_dma_t* xdma, bool circular)
{
    DMA_InitTypeDef DMA_InitStruct;
    DMA_StructInit(&DMA_InitStruct);

    DMA_DeInit(xdma->chnl);
    DMA_InitStruct.DMA_BufferSize = xdma->n_leds * COLOR_RGB_TYPE_WS2811_N_BITS;
    DMA_InitStruct.DMA_DIR = DMA_DIR_PeripheralDST;
    if (circular)
        DMA_InitStruct.DMA_Mode = DMA_Mode_Circular;
    else
        DMA_InitStruct.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStruct.DMA_MemoryBaseAddr = (uint32_t) xdma->buf;
    // 16 bit half word transfer
    DMA_InitStruct.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
    DMA_InitStruct.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStruct.DMA_PeripheralBaseAddr = xdma->dest;
    DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
    DMA_InitStruct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStruct.DMA_Priority = xdma->prio;

    xtimers_dma_irq_init(xdma);
    DMA_Init(xdma->chnl, &DMA_InitStruct);
}


static bool xtimers_dma_config(xtimer_dma_t* xdma)
{
  if (xdma)
  {
      if (0 == xdma->n_leds  )
      {
          log_msg("%s: size mandatory!\r\n", __FUNCTION__);
          return false;
      }
  }
  else
  {
      log_msg("%s: xdma is NULL?!\r\n", __FUNCTION__);
      return false;
  }

  if (NULL == xdma->buf)
  {
      xdma->buf = pvPortMalloc(xdma->n_leds_max * sizeof(COLOR_RGB_TYPE_WS2811_TIM_T));
      if (NULL == xdma->buf)
      {
          log_msg("%s: malloc failed\r\n", __FUNCTION__);
          return false;
      }
  }

  return true;
}


/**
 * Fills DMA buffer from given position start and converts n_elem RGB values
 * from given parameter rgb_buf with PWM values.
 *
 * @param rgb_buf
 * @param dma_start
 * @param n_elem
 */
inline static void xtimer_fill_dma(const COLOR_RGB_TYPE_T* rgb_buf, uint16_t dma_start, uint16_t n_elem)
{
    for (uint16_t cnt = 0; cnt < n_elem; cnt++)
    {
        const uint8_t*  rgb_color = (uint8_t*) &rgb_buf[cnt];
        uint8_t*        ws2811_color = ((uint8_t*) xdma1.buf) + (dma_start+cnt)*sizeof(COLOR_RGB_TYPE_WS2811_TIM_T);
        color_tim_rgb2ws2811(rgb_color, ((uint16_t*) ws2811_color));
    }
}



static void xtimers_dma_stop(xtimer_dma_t* xdma)
{
    // turn DMA off
    DMA_Cmd(xdma->chnl, DISABLE);

    // disconnect interrupt
    TIM_DMACmd(xdma->timer, TIM_DMA_Update, DISABLE);

    // disable interrupt
    DMA_ITConfig(xdma->chnl, DMA_IT_HT | DMA_IT_TC, DISABLE);
}


/**
 * Initializes DMA IRQ at interrupt controller (NVIC)
 * @param  xdma     The xtimers dma to use.
 */
static void xtimers_dma_irq_init(xtimer_dma_t* xdma)
{
    NVIC_InitTypeDef NVIC_InitStructure;

    /* Configure the SPI interrupt priority */
    NVIC_InitStructure.NVIC_IRQChannel = xdma->irq_chnl;
    NVIC_InitStructure.NVIC_IRQChannelPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}


/**
 * DMA Channel 1 IRQ Handler.
 *
 * We use double buffering & circular dma to convert rgb buffer to ws2811 compatible PWM waveforms.
 * Buffer handling is a bit tricky as we deal with different pointers to typedefs and
 * also basic types like uint8_t* / uint16_t*.
 *
 * If we have consumed all rgb buffer values we are still not finished:
 *   - switch from circular dma to normal dma mode
 *   - be sure, we have sufficient number of 0 values in dma buffer to reach at least 60 us pause time
 *     for ws2811 to activate received data.
 *
 */
void DMA1_Channel1_IRQHandler(void)
{
    uint32_t it_status = DMA1->ISR;

    /*
     DMA1_IT_GL1: DMA1 Channel1 global interrupt.
     DMA1_IT_TC1: DMA1 Channel1 transfer complete interrupt.
     DMA1_IT_HT1: DMA1 Channel1 half transfer interrupt.
     DMA1_IT_TE1: DMA1 Channel1 transfer error interrupt.
    */

    if (it_status & (DMA1_IT_TC1 | DMA1_IT_HT1))
    {
        DMA_ClearITPendingBit(DMA1_IT_GL1 | DMA1_IT_TC1 | DMA1_IT_HT1);

        long xHigherPriorityTaskWoken = pdFALSE;
        rgb_buf_t* p_rgb = xdma1.src;
        uint16_t pos = p_rgb->rgb_buf_pos;
        uint16_t n_left = p_rgb->rgb_buf_size - pos;
        uint16_t n_convert = 0;
        uint16_t dma_half_n_elems = xdma1.n_leds / 2;

        if (it_status & DMA1_IT_TC1)
        {
            /* Transfer complete interrupt */
#ifdef XTIMERS_DEBUG
            dbg_pin(0x1, true);
#endif
            if ((DMA1_Channel1->CCR & DMA_Mode_Circular) == 0)
            {
                /* everything completed: disable PWM, DMA */
                xtimers_dma_stop(&xdma1);
                TIM_Cmd(TIM17, DISABLE);

                /* signal user space task that we are finished */
                xSemaphoreGiveFromISR(xtimer_sem, &xHigherPriorityTaskWoken);
                if (xHigherPriorityTaskWoken)
                {
                    taskYIELD();
                }
#ifdef XTIMERS_DEBUG
                dbg_pin(0x1, false);
#endif
                return;
            }

            if (n_left >= dma_half_n_elems)
            {
                /* fill complete 2nd half of DMA buffer */
                n_convert = dma_half_n_elems;
            }
            else
            {
                if (n_left != 0)
                {
                    /* fill 2nd half of DMA buffer with what is left. */
                    n_convert = n_left;
                }
                else
                {
                    /* all source bytes consumed
                     * update DMA: switch from circular mode to normal mode
                     * next time we detect the bit and can leave DMA mode
                     */
                    DMA1_Channel1->CCR &= ~DMA_CCR_CIRC;
                }
            }

            if (n_convert > 0)
            {
                xtimer_fill_dma(p_rgb->rgb_buf+pos, dma_half_n_elems, n_convert);
            }

            if (n_convert != dma_half_n_elems)
            {
                // fill rest of buffer with zeros
                char* start = ((char*) xdma1.buf) + (dma_half_n_elems + n_convert) * sizeof(COLOR_RGB_TYPE_WS2811_TIM_T);
                size_t size = (dma_half_n_elems-n_convert) * sizeof(COLOR_RGB_TYPE_WS2811_TIM_T);
                memset(start, 0, size);
            }
            p_rgb->rgb_buf_pos += n_convert;
        }

        if (it_status & DMA1_IT_HT1)
        {
            /* Half Transfer complete interrupt */
#ifdef XTIMERS_DEBUG
            dbg_pin(0x2, true);
#endif
            if (n_left >= dma_half_n_elems)
            {
                /* fill complete 1st half of DMA buffer */
                n_convert = dma_half_n_elems;
            }
            else
            {
                if (n_left != 0)
                {
                    /* fill 1st half of DMA buffer with what is left. */
                    n_convert = n_left;
                }
                else
                {
                    /* all source bytes consumed
                     * update DMA: switch from circular mode to normal mode
                     * next time we detect the bit and can leave DMA mode
                     */
                    DMA1_Channel1->CCR &= ~DMA_CCR_CIRC;
                }
            }

            if (n_convert > 0)
            {
                xtimer_fill_dma(p_rgb->rgb_buf+pos, 0, n_convert);
            }

            if (n_convert != dma_half_n_elems)
            {
                // fill rest of dma buffer with zeros
                char* start = ((char*) xdma1.buf)+(n_convert*sizeof(COLOR_RGB_TYPE_WS2811_TIM_T));
                size_t size = (dma_half_n_elems-n_convert)*sizeof(COLOR_RGB_TYPE_WS2811_TIM_T);
                memset(start, 0, size);
            }
            p_rgb->rgb_buf_pos += n_convert;
        }
    }

    if (it_status & DMA1_IT_TE1)
    {
        /* error bit set, we don't like this, what should we do ? */
        DMA_ClearITPendingBit(DMA1_IT_GL1 | DMA1_IT_TE1 );
        log_msg("%s: DMA1_IT_TE1! Expect some weird stuff happening now ...\r\n", __FUNCTION__);
    }

#ifdef XTIMERS_DEBUG
    dbg_pin(0x1|0x2, false);
#endif
}


/**
 * Initializes xtimers facility.
 *
 * @param frequency     Base frequency used for PWM modulation
 *
 * @return      true    if everything all right
 *              false   if initialization failed
 */
bool xtimers_init(uint32_t frequency)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    TIM_OCInitTypeDef  TIM_OCInitStructure;
    TIM_BDTRInitTypeDef TIM_BDTRInitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

    /* initialize semaphores */
    xtimer_mutex = xSemaphoreCreateRecursiveMutex();
    vSemaphoreCreateBinary(xtimer_sem);

    /* initialize HW */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;

    /* GPIOB Config: TIM17 Ch1 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    /* Connect TIM pins to AF2 */
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_2);

    /* Time Base configuration */
    TIM_DeInit(TIM17);
    TIM_TimeBaseStructure.TIM_Prescaler = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_Period = frequency;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;

    TIM_TimeBaseInit(TIM17, &TIM_TimeBaseStructure);

    /* Channel 1, 2 and 3 Configuration in PWM mode */
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;
    TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;
    TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCIdleState_Reset;

    TIM_OCInitStructure.TIM_Pulse = 0;
    TIM_OC1Init(TIM17, &TIM_OCInitStructure);

    TIM_OC1PreloadConfig(TIM17, TIM_OCPreload_Enable);

    /* Automatic Output enable, Break, dead time and lock configuration*/
    TIM_BDTRStructInit(&TIM_BDTRInitStructure);
    TIM_BDTRInitStructure.TIM_OSSRState = TIM_OSSRState_Enable;
    TIM_BDTRInitStructure.TIM_OSSIState = TIM_OSSIState_Enable;
    TIM_BDTRInitStructure.TIM_LOCKLevel = TIM_LOCKLevel_1;
    TIM_BDTRInitStructure.TIM_DeadTime = 0;
    TIM_BDTRInitStructure.TIM_Break = TIM_Break_Enable;
    TIM_BDTRInitStructure.TIM_BreakPolarity = TIM_BreakPolarity_High;
    TIM_BDTRInitStructure.TIM_AutomaticOutput = TIM_AutomaticOutput_Enable;

    TIM_BDTRConfig(TIM17, &TIM_BDTRInitStructure);

    if (false == xtimers_dma_config(&xdma1))
    {
        log_msg("%s: giving up \r\n");
        return false;
    }

    return true;
}


/**
 * Start PWM DMA timer.
 * This call should block until all the bytes have been transfered over the wire.
 *
 * @param rgb_buf
 * @param rgb_n_elems
 *
 * @return  true    everything allright
 *          false   timeout occured while waiting for semaphore (10 ticks)
 */
bool xtimers_start(const COLOR_RGB_TYPE_T* rgb_buf, size_t rgb_buf_elems)
{
    portTickType ticks = 10;        // in ms

    /* no concurrent access */
    if (xSemaphoreTakeRecursive(xtimer_mutex, ticks) != pdTRUE)
    {
        return false;
    }

    xtimer_dma_t* xdma = &xdma1;

    rgb_buf_t* p_rgb = xdma->src;
    p_rgb->rgb_buf = rgb_buf;
    p_rgb->rgb_buf_size = rgb_buf_elems;
    p_rgb->rgb_buf_pos = 0;

    bool should_db = true;

    // we transfer n leds x number of bits per RGB LED (3x8 = 24bits)
    uint16_t dma_buf_elems = 0;
    if (rgb_buf_elems <= xdma->n_leds_max)
    {
        // we don't need to round robin
        if (rgb_buf_elems <= xdma->n_leds_max - 2)
        {
            /* We continue serving more zero values until we have at least 60us dead time of
             * 48*1,25us (48 bit cycles == 2 complete LED values)
             * XXX DS: This has to be set according to base frequency of PWM
             */
            xdma->n_leds = rgb_buf_elems + 2;
            should_db = false;
        }
        else
        {
            xdma->n_leds = rgb_buf_elems;
        }
        dma_buf_elems = rgb_buf_elems;
    }
    else
    {
        // we have to round robin via circular buffer and use double buffering
        xdma->n_leds = xdma->n_leds_max;
        dma_buf_elems = xdma->n_leds_max;
    }
    memset(xdma->buf, 0, xdma->n_leds_max * sizeof(COLOR_RGB_TYPE_WS2811_TIM_T));
    xtimer_fill_dma(rgb_buf, 0, dma_buf_elems);

#ifdef XTIMERS_EXTREME_DEBUG
log_msg("%s: xdma->buf start 0x%x, end 0x%x\r\n", __FUNCTION__, xdma->buf, xdma->buf+xdma->n_leds  );
sram_dump(xdma->buf, xdma->buf+xdma->n_leds  );
log_msg("%s: p_rgb->rgb_buf 0x%x, end 0x%x\r\n", __FUNCTION__, p_rgb->rgb_buf, p_rgb->rgb_buf+p_rgb->rgb_buf_size);
sram_dump(p_rgb->rgb_buf, p_rgb->rgb_buf+p_rgb->rgb_buf_size);
#endif
    p_rgb->rgb_buf_pos += dma_buf_elems;
    xtimers_update_dma(xdma, should_db);

    // enable interrupt: dbl buffering
    if (should_db)
        DMA_ITConfig(xdma->chnl, DMA_IT_HT | DMA_IT_TC, ENABLE);
    else
        DMA_ITConfig(xdma->chnl, DMA_IT_TC, ENABLE);

    // connect interrupt
    TIM_DMACmd(xdma->timer, TIM_DMA_Update, ENABLE);

    // turn DMA on
    DMA_Cmd(xdma->chnl, ENABLE);

    // enable timer
    TIM_Cmd(xdma->timer, ENABLE);
    TIM_CtrlPWMOutputs(xdma->timer, ENABLE);

    /* wait until ISR has finished */
    if (xSemaphoreTake(xtimer_sem, ticks) != pdTRUE)
    {
        log_msg("%s: Hey timeout?!\r\n", __FUNCTION__);
    }
    xSemaphoreGiveRecursive(xtimer_mutex);
    return true;
}

/* EOF */
