/**
 * @file
 *****************************************************************************
 * @title   diagctl.c
 * @author  Daniel Schnell (deexschs)
 *
 * @brief  diagctrl application
 *******************************************************************************/

#include <stddef.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>
/* our own implementation of printf and sprintf */
#include "printf.h"

#include "stm32f0xx_syscfg.h"
#include "stm32f0xx.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_exti.h"
#include "stm32f0xx_misc.h"
#include "stm32f0xx_conf.h"
#include "stm32f0xx_usart.h"
#include "stm32f0xx_adc.h"
#include "stm32f0xx_rtc.h"

#include "usbd_cdc_core.h"
#include "usbd_usr.h"

#include <FreeRTOS.h>
#include <task.h>

#include "conversions.h"
#include "drivers.h"
#include "logger.h"
#include "os.h"
#include "usart.h"
#include "spi.h"
#include "xtimers.h"

#include "monitor.h"

#include "dbg_pin.h"

#include "worker_queue.h"

#include "l3gd20.h"
extern void hooks_get(void);


#define CFG_STACK_SIZE 150      // multiplied with 4 !
#define BINGO       do { } while(true)

/**
 * USB device handle for VCOM
 */
USB_CORE_HANDLE  USB_Device_dev;


void turn_all_leds_on(bool ok) {
	BitAction state = Bit_RESET;
	uint16_t pins[] = {GPIO_Pin_6,GPIO_Pin_7,GPIO_Pin_8,GPIO_Pin_9};
	if (ok){
		state = Bit_SET;
	}
	for(unsigned int i = 0; i < sizeof(pins); i++) {
		GPIO_WriteBit(GPIOC, pins[i], state);
	}
}

void scale_angle(float *angle) {
	if (*angle < 0.0f) {
		 *angle += 360.0f;
	} else if (*angle > 359.0f) {
		 *angle -= 360.0f;
	}
}

/**
 * General purpose task that executes with priority 3
 */
static void worker_task(void* arg P_UNUSED)
{

	 L3GD20Data_t gyro; 		 /* used to read data from l3gd20 gyroscope */
	 L3GD20Data_t gyro_zero;	 /* zero-rate level */


	 GPIO_InitTypeDef  GPIO_InitStructure;

	/* Configure the GPIO_LED pin */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	/* turn off all LEDs */
	turn_all_leds_on(false);

	uint8_t data = 0;

	/* check if we can communicate */
	if( 1 != l3gd20_read_register(L3GD20_REGISTER_WHO_AM_I,&data)){
		BINGO;
	}
	 /* read ctrl register 1 */
	if ( 1 != l3gd20_read_register(L3GD20_REGISTER_CTRL_REG1,&data)) {
		BINGO;
	}

	data |= 1<<3; /* enable device */
	if(1 != l3gd20_write_register(L3GD20_REGISTER_CTRL_REG1,data)){
		BINGO;
	}

	/* initialize the variables */
	gyro.x_axis = 0;
	gyro.y_axis = 0;
	gyro.z_axis = 0;
	/* set zero rate levels to zero */
	gyro_zero.x_axis = 0;
	gyro_zero.y_axis = 0;
	gyro_zero.z_axis = 0;

	/* turn on all LEDs to show we are ready with teaching */
	turn_all_leds_on(true);

	int cnt = 0;
	/* wait until we have successfully red all values for average */
	while (cnt < 200 ) {
		if ( 1 == l3gd20_get_result(&gyro) ) {
			gyro_zero.x_axis += gyro.x_axis;
			gyro_zero.y_axis += gyro.y_axis;
			gyro_zero.z_axis += gyro.z_axis;
			cnt++;
		}
		vTaskDelay( 10/portTICK_PERIOD_MS );
	}

	/* calculate the average for zero rate */
	gyro_zero.x_axis /= cnt;
	gyro_zero.y_axis /= cnt;
	gyro_zero.z_axis /= cnt;

	log_msg("zero value [ %d, %d, %d ]\r",(int)gyro_zero.x_axis,(int)gyro_zero.y_axis,(int)gyro_zero.z_axis);

	/* turn off all LEDs to show we are ready with teaching and ready to rumble */
	turn_all_leds_on(false);

	/* On start position all angles are zero */
	float angle_x = 0.0f;
	float angle_y = 0.0f;
	float angle_z = 0.0f;

	float r_x = 0.0f;
	float r_y = 0.0f;
	float r_z = 0.0f;

	do
	{
		if( 1 == l3gd20_get_result(&gyro)) {

			/**
			 * R_t = SC*(R_m-R_0)
			 *
			 * R_t  - is the true angular rate in dps
			 * SC   - is the scale factor given in dps/digit
			 * R_m  - is the gyroscope raw measurement
			 * R_0  - is the zero rate measurement. This is the output if no angular rate is applied.
			 */

			r_x = L3GD20_SENSITIVITY_250_DPS * (gyro.x_axis - gyro_zero.x_axis);
			r_y = L3GD20_SENSITIVITY_250_DPS * (gyro.y_axis - gyro_zero.y_axis);
			r_z = L3GD20_SENSITIVITY_250_DPS * (gyro.z_axis - gyro_zero.z_axis);


			/*
			 * Integrating the dps values over time gives the angular displacement
			 * or how many degrees rotated.
			 *
			 * dx = dx + h * R_x * SC_x
			 *
			 * dx  - Is the gyroscope’s angular displacement on the X-axis given in degrees
			 * h   - Is the sampling period given in seconds.
			 *       If ODR = 100 Hz, then h = 0.01 s
			 * R_t - Is gyroscope’s raw data given in dps after removal of the
			 *       zero-rate level offset and the threshold an multiplied by sensitivity.
			 */

			angle_x += r_x * L3GD20_DT_ODR_95;
			angle_y += r_y * L3GD20_DT_ODR_95;
			angle_z += r_z * L3GD20_DT_ODR_95;

			/* scaling angles between 0 - 360 degrees */
			scale_angle(&angle_x);
			scale_angle(&angle_y);
			scale_angle(&angle_z);

			/* print out every 50th sample */
			if((++cnt%50)== 0 )
			{
				log_msg("angle [ %d, %d, %d] \r",(int)floor(angle_x),(int)floor(angle_y),(int)floor(angle_z));
				cnt = 0;
			}
		}
		vTaskDelay( 10/portTICK_PERIOD_MS );
	} while (true);
}


/**
 * @brief  Main function of STM32F072 Demo software
 * @param  None
 * @retval None
 */
int main(void)
{
    // dummy function to resolve hook functions in libioc.a
    hooks_get();

    /* Enable clocks */
    drivers_clocks_init();

    /* Initializes peripherals */
    if (false == usart1_init())
    {
        BINGO;
    }
    /* Initialize debug pin facility */
    dbg_pin_init();

    /* spi communication */
    init_spi2();

    /*l3gd20 init*/
    l3gd20_init();
    printf("\r\n");
    printf("+===================+\r\n");
    printf("| STM32F072 Demo    |\r\n");
    printf("+===================+\r\n");
    printf("\r\n");
    printf("Press 'h' for help\r\n");

    /* logging is only available after scheduler is started */
    if (false == log_init())
    {
        BINGO;
    }

    /* initialize command monitor */
    if (false == monitor_init())
    {
        BINGO;
    }

    /* The Application layer has only to call USBD_Init to
      initialize the USB low level driver, the USB device library, the USB clock
      ,pins and interrupt service routine (BSP) to start the Library*/
      USBD_Init(&USB_Device_dev,
                &USR_desc,
                &USBD_CDC_cb,
                &USR_cb);


    xTaskHandle th = NULL;
    /* worker task */
    xTaskCreate(worker_task, (const char * const) "Worker", CFG_STACK_SIZE-25,
                (void *) 0, 2, &th);
    os_add_tcb(th);

    vTaskStartScheduler();

    // is only executed if task creation has not been successful
    BINGO;
}




